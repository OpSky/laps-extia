import {PhraseTrou} from './phrase';

export const PHRASETROUS: PhraseTrou[] =
    [
        {phrase : 'Acte ', mots: ['d\'identité', 'de naissance', 'de famille'], bonmot: 'de naissance'},
        {phrase : 'Carte ', mots: ['d\'identité', 'de domicile', 'de famille'], bonmot: 'd\' identité'}
    ];
