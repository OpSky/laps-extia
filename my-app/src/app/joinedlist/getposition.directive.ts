import { Directive,Input,Output,ElementRef,Renderer} from '@angular/core';
import { EventEmitter } from 'events';

@Directive({
  selector:"[getRef]",
  host:{
    '(click)':"show()"
  }
})
export class GetPositionDirective{

    @Output() ref = new EventEmitter();

  constructor(private el:ElementRef){ }
  show(){
    /*console.log(JSON.stringify(this.el.nativeElement));
    console.log(this.el.nativeElement.offsetLeft);*/
    var x = this.el.nativeElement.offsetLeft;
    this.ref.emit(x);
    
  }

}