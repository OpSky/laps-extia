import { Component, ElementRef, Input, ViewChildren } from '@angular/core';
import { GetPositionDirective } from './getposition.directive'
import { ViewChild } from '@angular/core/src/metadata/di';

//declare var jsPlumb:any;

@Component ({
    selector: 'joinedlist',
    templateUrl : 'joinedlist.component.html',
    styleUrls: ['joinedlist.component.css']
})
export class JoinedListComponent {
    lefties1= ["acte","relevé", "avis", "attestation", "certificat", "bulletin", "livret", "carte", "notification", "déclaration"];
    righties1= ["de naissance/de mariage","d’identité bancaire", "d’imposition/non imposition", "de Pôle emploi", "médical", "de salaire", "de famille", "d’identité", "d’attribution/de rejet AAH", "sur l’honneur"]
    //goodanswers = ["acte de naissance/de mariage", "relevé d'identité bancaire", "certificat médical", "livret de famille",]
    actualanswers=[];
    goodanswers = ["certificat médical"]

    lefties= this.shuffle(this.lefties1);
    righties = this.shuffle(this.righties1);
    answers = [];
    answer1="";
    answer2="";
    correct: boolean=false;
    play: boolean=false;

    el:any;
    index1=0;
    index2=0;
    @ViewChildren('selector') selectors: ElementRef;
    @ViewChildren('selector2') selectors2: ElementRef;

    /*line = {
        x1: 0,
        x2: 200,
        y1: 0,
        y2: 200
    }*/
    lines = [];


    constructor(elRef: ElementRef) {
        //this.lines.push(this.line);
        
    }

    ngAfterViewInit() {
        console.log(this.selectors);
        console.log(this.selectors2);
        console.log(this.lines)
  /*      jsPlumb.ready(function() {
            jsPlumb.connect({
                source:'#test1',
                target:'#5',
                paintStyle:{strokeWidth:15,stroke:'rgb(243,230,18)'},
                endpointStyle:{fill:'rgb(243,229,0)'}
              });        
            })*/
    }


    shuffle(array) {
        var currentIndex = array.length, temporaryValue, randomIndex;
      
        // While there remain elements to shuffle...
        while (0 !== currentIndex) {
      
          // Pick a remaining element...
          randomIndex = Math.floor(Math.random() * currentIndex);
          currentIndex -= 1;
      
          // And swap it with the current element.
          temporaryValue = array[currentIndex];
          array[currentIndex] = array[randomIndex];
          array[randomIndex] = temporaryValue;
        }
      
        return array;
      }

      selectAnswer1(w, index1) {
          this.answer1=w;
          this.index1=index1;
          this.addAnswer();
          var a = this.selectors['_results'][this.index1].nativeElement;
            console.log(a.offsetLeft);
            console.log(a.offsetTop);
            console.log(a.offsetParent);
        }
    
      selectAnswer2(w, index2) {
          this.answer2=w;
          this.index2=index2;
          this.addAnswer();
          var a = this.selectors2['_results'][this.index2].nativeElement;
          console.log(a.offsetLeft);
          console.log(a.offsetTop);
        }

      addAnswer() {
          if(this.answer1 && this.answer2) {
              this.removeFromAnswers(this.answer1, this.answer2, this.lines);
              this.answers.push(this.answer1 + ' ' + this.answer2);
              var a = this.selectors['_results'][this.index1].nativeElement;
              var b = this.selectors2['_results'][this.index2].nativeElement;
              var line = {
                  x1: a.offsetLeft-50,
                  y1: a.offsetTop,
                  x2: b.offsetLeft+300,
                  y2: b.offsetTop
              }
              
              this.lines.push(line);
              this.answer1 = "";
              this.answer2 = "";
            }
      }

      removeFromAnswers(a1, a2, l) {
        var c;  
        for(var i=0; i<this.answers.length; i++) {
              c = this.answers[i];
              if(c.indexOf(a1) !== -1 || c.indexOf(a2) !== -1) {
                  this.answers.splice(i, 1);
                  this.lines.splice(i, 1);
               }
          }
          console.log(this.answers);
      }

      getPosition( el2: ElementRef ) {
        var x = 0;
        var y = 0;
        this.el= el2.nativeElement;
        console.log(this.el);
        
        while( this.el && !isNaN( this.el.offsetLeft ) && !isNaN( this.el.offsetTop ) ) {
        x += this.el.offsetLeft - this.el.scrollLeft;
        y += this.el.offsetTop - this.el.scrollTop;
        this.el = this.el.offsetParent;
        }
        console.log(x + " " + y);
        return { top: y, left: x };
    }

      checkAnswers() {
          this.play = true;
          for(var i=0;i<this.answers.length;i++) {
            if(!this.goodanswers.includes(this.answers[i])) {
                this.correct=false;
                break;
            }
            else {
                this.correct=true;
            }
          }
      }
 }